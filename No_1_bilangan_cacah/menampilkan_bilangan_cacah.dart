import 'dart:io';

void main() {
  cekangka();
}

void cekangka() {
  print("Masukkan banyaknya bilangan yang ditampilkan :");

  int? n = int.parse(stdin.readLineSync()!);
  int angka = 0;
  int batas = 0;
  do {
    ++angka;
    if (angka % 3 == 0) {
      print("$angka");
      batas++;
    } else if (angka % 7 == 0) {
      print("$angka");
      batas++;
    } else if (angka % 3 == 0 && angka % 7 == 0) {
      print("Z");
      batas++;
    }
  } while (batas < n);
}
