import 'dart:io';

void main() {
  int num = validasipwd();
  switch (num) {
    case 1:
      {
        print("minmal 8 karakter dan maksimal 32 karakter");
      }
      break;
    case 2:
      {
        print("Harus memiliki 2 atau lebih simbol");
      }
      break;
    case 3:
      {
        print("Harus memiliki angka");
      }
      break;
    case 4:
      {
        print("Harus memiliki huruf kapital dan huruf kecil");
      }
      break;
    case 5:
      {
        print("Karakter awal tidak boleh angka");
      }
      break;
    case 6:
      {
        print("Tidak boleh memiliki 3 angka berurutan");
      }
      break;
    default:
      {
        print("password valid");
      }
      break;
  }
}

int validasipwd() {
  print("Enter your password?");

  String? password = stdin.readLineSync();

  String spcchar = r'[!@#$%^&*(),.?":{}|<>]';
  String number = r'[0-9]';
  String uppercase = r'[A-Z]';
  String lowercase = r'[a-z]';
  bool cek1 = true;
  int minpass = 8;
  int maxpass = 32;
  int ceknum = 0;

  //minmal 8 karakter dan maksimal 32 karakter
  bool lengthpass = password!.length >= minpass && password.length <= maxpass;
  if (!lengthpass) {
    return 1;
  }

  //Harus memiliki 2 atau lebih simbol
  RegExp regExpspcchar = new RegExp(spcchar);
  //print(regExpspcchar.hasMatch(password));
  //print(regExpspcchar.allMatches(password).length);
  bool ceksymbol =
      regExpspcchar.allMatches(password).length == 2 ? true : false;
  if (!ceksymbol) {
    return 2;
  }
  //Harus memiliki angka
  RegExp regExpnumber = new RegExp(number);
  //print(regExpnumber.hasMatch(password));
  //print(password.contains(regExpnumber));
  if (!password.contains(regExpnumber)) {
    return 3;
  }
  //Harus memiliki huruf kapital dan huruf kecil
  RegExp regExpuppercase = new RegExp(uppercase);
  //print(regExpuppercase.hasMatch(password));
  RegExp regExplowercase = new RegExp(lowercase);
  //print(regExplowercase.hasMatch(password));
  if (!regExplowercase.hasMatch(password) &&
      !regExpuppercase.hasMatch(password)) {
    return 4;
  }
  //Karakter awal tidak boleh angka
  String strawal = password[0];
  if (strawal.contains(regExpnumber)) {
    print("angka");
    cek1 = false;
    return 5;
  }
  //Tidak boleh memiliki 3 angka berurutan
  List result = password.split('');
  //print(result);
  for (int i = 0; i < result.length; i++) {
    if (result[i].contains(regExpnumber)) {
      ceknum++;
    } else {
      ceknum = 0;
    }
  }
  if (ceknum == 3) {
    return 6;
  }
  return 0;
}
