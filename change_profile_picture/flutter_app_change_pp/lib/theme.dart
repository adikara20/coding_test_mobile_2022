import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color defaultColor = const Color(0xff002984);

TextStyle defaultText = GoogleFonts.nunito();
