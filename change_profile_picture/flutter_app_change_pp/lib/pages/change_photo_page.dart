import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_change_pp/theme.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

class changePhotoPage extends StatefulWidget {
  const changePhotoPage({Key? key}) : super(key: key);

  @override
  _changePhotoPageState createState() => _changePhotoPageState();
}

class _changePhotoPageState extends State<changePhotoPage> {
  File? setimage;
  File? setimageReturn;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Ganti Foto Profil",
          style: defaultText,
        ),
        backgroundColor: defaultColor,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              setimage == null
                  ? Container(
                      height: 343,
                      width: 343,
                      //color: Color(0xffE0E0E0),
                      decoration: BoxDecoration(
                        color: Color(0xffE0E0E0),
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0xffE0E0E0),
                            blurRadius: 10,
                            offset: Offset(5, 5),
                          ),
                        ],
                      ),
                    )
                  // : FutureBuilder(
                  //     future: setimage,
                  //     builder: (BuildContext context, AsyncSnapshot snapshot) {
                  //       setimageReturn = snapshot.data;
                  //       if (snapshot.connectionState == ConnectionState.done &&
                  //           snapshot.data != null) {
                  //         return Image.file(
                  //           snapshot.data!,
                  //           width: 400,
                  //           height: 400,
                  //         );
                  //       } else {
                  //         return Container(
                  //           height: 343,
                  //           width: 343,
                  //           //color: Color(0xffE0E0E0),
                  //           decoration: BoxDecoration(
                  //             color: Color(0xffE0E0E0),
                  //             borderRadius: BorderRadius.circular(8),
                  //             boxShadow: [
                  //               BoxShadow(
                  //                 color: Color(0xffE0E0E0),
                  //                 blurRadius: 10,
                  //                 offset: Offset(5, 5),
                  //               ),
                  //             ],
                  //           ),
                  //         );
                  //       }
                  //     },
                  //   ),
                  : Image.file(
                      setimage!,
                      width: 343,
                      height: 343,
                      fit: BoxFit.contain,
                    ),
              SizedBox(
                height: 34,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size.fromHeight(40),
                  primary: Color(0xff006C84),
                  shadowColor: Color(0xff006C84),
                ),
                onPressed: () => pickImage(ImageSource.camera),
                child: Center(
                  child: Text(
                    "Ambil dari kamera",
                    style: defaultText.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              // Container(
              //   width: 341,
              //   height: 40,
              //   //color: Color(0xffE0E0E0),
              //   decoration: BoxDecoration(
              //     color: Color(0xff006C84),
              //     borderRadius: BorderRadius.circular(8),
              //     boxShadow: [
              //       BoxShadow(
              //         color: Color(0xff006C84),
              //         blurRadius: 5,
              //         offset: Offset(5, 5),
              //       ),
              //     ],
              //   ),
              //   child: Center(
              //     child: Text(
              //       "Ambil dari kamera",
              //       style: defaultText.copyWith(
              //         fontSize: 12,
              //         fontWeight: FontWeight.w700,
              //         color: Colors.white,
              //       ),
              //     ),
              //   ),
              // ),
              SizedBox(
                height: 16,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size.fromHeight(40),
                  primary: Color(0xff006C84),
                  shadowColor: const Color(0xff006C84),
                ),
                onPressed: () => pickImage(ImageSource.gallery),
                child: Center(
                  child: Text(
                    "Ambil dari Gallery",
                    style: defaultText.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Material(
        child: InkWell(
          onTap: () {
            Navigator.pop(context, setimage);
            uploadImage(setimage!);
          },
          child: SizedBox(
            height: 56,
            width: double.infinity,
            child: Center(
              child: Text(
                "Simpan",
                style: defaultText.copyWith(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
        ),
        color: defaultColor,
      ),
    );
  }

  Future pickImage(ImageSource source) async {
    final getimage = await ImagePicker().pickImage(source: source);
    if (getimage?.path != null) {
      // setState(() {
      //   //setimage = File(getimage!.path);
      // });
      cropImage(getimage?.path);
    }
  }

  Future cropImage(imagePath) async {
    final croppedimage = await ImageCropper.cropImage(
      sourcePath: imagePath,
      maxHeight: 512,
      maxWidth: 512,
      compressFormat: ImageCompressFormat.jpg,
      aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
    );
    if (croppedimage?.path != null) {
      setimage = croppedimage;
      setState(() {});
    }
  }

  uploadImage(File file) async {
    var request = http.MultipartRequest("PUT",
        Uri.parse("https://staging-satrio.kelaspintar.co.id/lpt-api/api/file"));

    //request.fields['title'] = "FotoProfile";
    request.headers['Authorization'] =
        "wTPb3I47TnooWoJijkUw65YIhp72X3YrE5fA+c27mZcJzEka6Uxp2jTV3qMabKESnxpFnARAWFE8NN79qcf3Dw==";

    http.MultipartFile multipartFile =
        await http.MultipartFile.fromPath("file", file.path);

    request.files.add(multipartFile);

    var response = await request.send();

    var responseData = await response.stream.toBytes();

    var result = String.fromCharCodes(responseData);

    print(result);
  }
}
