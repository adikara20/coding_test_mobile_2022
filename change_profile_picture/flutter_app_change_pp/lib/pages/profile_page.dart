import 'dart:io';

import 'package:flutter_app_change_pp/pages/change_photo_page.dart';
import 'package:flutter_app_change_pp/theme.dart';
import 'package:flutter/material.dart';

class profilePage extends StatefulWidget {
  const profilePage({Key? key}) : super(key: key);

  @override
  _profilePageState createState() => _profilePageState();
}

class _profilePageState extends State<profilePage> {
  File? imagesetReturn;
  File? imagefile;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Profil",
          style: defaultText,
        ),
        backgroundColor: defaultColor,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(17),
          child: Column(
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () async {
                      imagefile = await Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => changePhotoPage(),
                        ),
                      );
                      imagesetReturn = imagefile;
                      setState(() {});
                    },
                    child: imagesetReturn == null
                        ? Container(
                            height: 56,
                            width: 56,
                            //color: Color(0xffE0E0E0),
                            decoration: BoxDecoration(
                                color: Color(0xffE0E0E0),
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black54,
                                    blurRadius: 5,
                                    offset: Offset(5, 5),
                                  ),
                                ]),
                            child: Icon(
                              Icons.camera_alt_outlined,
                              size: 20,
                            ),
                          )
                        : Image.file(
                            imagesetReturn!,
                            width: 56,
                            height: 56,
                            fit: BoxFit.contain,
                          ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  RichText(
                    text: TextSpan(
                        text: 'John Doe',
                        style: defaultText.copyWith(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.w700),
                        children: <TextSpan>[
                          TextSpan(
                            text: '\nKelas 10 IPA 8',
                            style: defaultText.copyWith(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),
                          )
                        ]),
                  ),
                ],
              ),
              // ignore: prefer_const_constructors
              SizedBox(
                height: 16,
              ),
              Expanded(
                child: ListView(
                  physics: ScrollPhysics(),
                  //padding: const EdgeInsets.all(8),
                  children: <Widget>[
                    Container(
                      height: 196,
                      width: 341,
                      decoration: BoxDecoration(
                        color: defaultColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 196,
                      width: 341,
                      decoration: BoxDecoration(
                        color: defaultColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 196,
                      width: 341,
                      decoration: BoxDecoration(
                        color: defaultColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 196,
                      width: 341,
                      decoration: BoxDecoration(
                        color: defaultColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
